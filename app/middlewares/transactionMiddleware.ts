import { Dispatch } from 'react';
import { Transaction } from '../redux/store/types';
import TransactionEntity from '../database/entities/TransactionEntity';
import { addTransactionCreator } from '../redux/actions/transaction/transactionActionCreator';

export const insertTransaction = (transaction: Transaction) => {
  return function (dispatch: Dispatch<any>) {
    const transactionEntity = new TransactionEntity();
    transactionEntity.amount = transaction.amount;
    transactionEntity.description = transaction.description;
    transactionEntity.isIncomes = transaction.isIncomes;
    transactionEntity.date = transaction.date;
    transactionEntity.userId = transaction.userId;
    transactionEntity.categoryId = transaction.categoryId;
    transactionEntity.save();
    transaction.id = transactionEntity.id;
    dispatch(addTransactionCreator(transaction));
  };
};
