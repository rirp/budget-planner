import { Dispatch } from 'react';

import PersonEntity from '../database/entities/PersonEntity';
import { Order } from '../database/orm/types';
import { createUser, createUsers } from '../redux/actions/user/UserActionsCreators';
import { User } from '../redux/store/types';

export function saveUser(user: User) {
  return function (dispatch: Dispatch<any>) {
    const person = new PersonEntity();
    person.name = user.name;
    person.currentBalance = user.currentBalance;
    person.save();

    user.id = person.id;
    dispatch(createUser(user));
  };
}

export function getUsers() {
  return async function (dispatch: Dispatch<any>) {
    let users = await PersonEntity.findBy<User>({ limit: 1, order: Order.ACS });
    if (users) {
      dispatch(createUsers(users));
    }
  };
}
