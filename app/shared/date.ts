const today = () => {
  const today = new Date();
  return today.toISOString().split('T')[0];
};

const yesterday = (): string => {
  const yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  return yesterday.toISOString().split('T')[0];
};

const isToday = (date: string): boolean => {
  return date === today();
};

const isYesterday = (date: string): boolean => {
  return date === yesterday();
};

export { today, yesterday, isToday, isYesterday };
