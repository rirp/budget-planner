import React, { useState } from 'react';
import { Input } from '../..';
import { getCurrencyFormat, cleanNumber } from '../../../format';

export default CurrencyInput = (props) => {
  const reg = new RegExp(/^\d+$/);
  const [value, setValue] = useState(0);

  function formatValue(value) {
    if (value == 0) {
      return '$ 0';
    } else if (value === '' || value === NaN || !reg.test(value)) {
      return '';
    } else {
      return getCurrencyFormat(value);
    }
  }

  function onFormatValue(value) {
    const cleaned = cleanNumber(value);
    const formated = formatValue(cleaned);

    setValue(formated);
    props.onChangeValue(cleaned, formated);
  }

  return <Input number value={value} onChangeText={(value) => onFormatValue(value)} {...props} />;
};
