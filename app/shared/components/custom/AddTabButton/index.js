import React, { useState } from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Entypo';
import styles from './styles';
import { Button, Text } from '../..';
import { theme } from '../../../constants';
import { useNavigation } from '@react-navigation/native';

export default AddTabButton = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation();

  return (
    <>
      <Button
        onPress={() => {
          setModalVisible(true);
        }}
        style={styles.buttonStyle}
      >
        <Icon name="wallet" size={40} color={theme.colors.white} />
      </Button>
      <View>
        <Modal
          backdropOpacity={0.3}
          isVisible={modalVisible}
          onBackdropPress={() => setModalVisible(false)}
          style={styles.contentView}
        >
          <View style={styles.content}>
            <Text h3 white>
              Select an option to add
            </Text>
            <Button
              style={styles.incomeButton}
              onPress={() => {
                setModalVisible(false);
                navigation.push('AddTransaction', { isIcome: true });
              }}
            >
              <Text h3 white>
                Income
              </Text>
            </Button>
            <Button
              style={styles.expensesButton}
              onPress={() => {
                setModalVisible(false);
                navigation.push('AddTransaction', { isIcome: false });
              }}
            >
              <Text h3 color="white">
                Expenses
              </Text>
            </Button>
          </View>
        </Modal>
      </View>
    </>
  );
};
