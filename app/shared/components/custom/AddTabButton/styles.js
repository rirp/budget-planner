import { StyleSheet } from 'react-native';
import { theme } from '../../../constants';

const styles = StyleSheet.create({
  content: {
    backgroundColor: theme.colors.darkGray,
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 17,
    borderTopLeftRadius: 17,
  },
  contentTitle: {
    fontSize: 20,
    marginBottom: 12,
  },
  contentView: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  buttonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    height: 70,
    borderRadius: 100,
    backgroundColor: theme.colors.primary,
    marginTop: -30,
    shadowColor: '#7F58FF',
    shadowRadius: 5,
    shadowOffset: { height: 10 },
    shadowOpacity: 0.3,
    borderWidth: 3,
    borderColor: theme.colors.white,
  },
  incomeButton: {
    marginTop: 20,
    backgroundColor: theme.colors.primary,
    width: '100%',
    alignItems: 'center',
  },
  expensesButton: {
    backgroundColor: 'tomato',
    width: '100%',
    alignItems: 'center',
  },
});

export default styles;
