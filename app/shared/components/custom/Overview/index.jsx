import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { Block, Card, Text } from '../..';
import { getCurrencyFormat } from '../../../format';
import { theme } from '../../../constants';

const Overview = ({ currentBalanace, style }) => {
  const currentMonth = new Date().toLocaleString('en-us', { month: 'long' });

  return (
    <TouchableOpacity>
      <Card center shadow color={theme.colors.darkGray}>
        <Text h1 white>
          {currentMonth}
        </Text>
        <Text caption white style={{ marginTop: 5 }}>
          Current balance
        </Text>
        <Text title white style={{ marginBottom: 5 }}>
          {getCurrencyFormat(currentBalanace)}
        </Text>
        <Block row center>
          <Icon name="arrow-up-circle" size={35} color="green" style={{ marginRight: 5 }} />
          <Block>
            <Text caption bold white>
              Incomes
            </Text>
            <Text title white>
              $ 300.000
            </Text>
          </Block>
          <Icon name="arrow-down-circle" size={35} color="#CA3433" style={{ marginRight: 5 }} />
          <Block>
            <Text caption bold white>
              Expenses
            </Text>
            <Text title white>
              $ 300.000
            </Text>
          </Block>
        </Block>
      </Card>
    </TouchableOpacity>
  );
};

export default Overview;
