import { StyleSheet } from 'react-native';
import { theme } from '../../../constants';

const styles = StyleSheet.create({
  button: {
    borderWidth: 1,
    borderColor: theme.colors.white,
    borderRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginHorizontal: 3,
    justifyContent: 'center',
  },
});

export default styles;
