import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Text } from '../..';
import styles from './styles';

type RenderOptionParams = {
  title: string;
  isSelected: boolean;
  onPressed: () => void;
};

const AddTransactionDateOption = ({ title, isSelected, onPressed }: RenderOptionParams) => {
  return (
    <TouchableOpacity
      style={[styles.button, isSelected ? { borderColor: '#CA3433' } : {}]}
      onPress={onPressed}
    >
      <Text body white>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default AddTransactionDateOption;
