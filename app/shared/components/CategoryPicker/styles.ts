import { StyleSheet } from 'react-native';
import { theme } from '../../constants';

export default StyleSheet.create({
  category: {
    borderWidth: 1,
    borderColor: 'gold',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginTop: 8,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-start',
    paddingRight: 15,
  },
  content: {
    backgroundColor: theme.colors.darkGray,
    padding: 20,
    borderTopRightRadius: 17,
    borderTopLeftRadius: 17,
  },
  contentView: {
    justifyContent: 'flex-end',
    margin: 0,
  },
});
