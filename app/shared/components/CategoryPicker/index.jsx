import React, { useState } from 'react';
import { View } from 'react-native';
import { FlatList, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import { Text } from '..';
import styles from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import { theme } from '../../constants';

const CategoryView = ({ categories, style, onCategorySelected }) => {
  const [viewData, setViewData] = useState({ isModalVisible: false, categorySelected: 0 });

  const itemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: '100%',
          backgroundColor: theme.colors.gray,
          marginVertical: 3,
        }}
      />
    );
  };

  const renderOptions = () => {
    return (
      <View style={styles.content}>
        <FlatList
          data={categories}
          ItemSeparatorComponent={() => itemSeparator()}
          renderItem={({ item, index }) => renderOption(item, index)}
        />
      </View>
    );
  };

  const renderOption = (category, index) => {
    return (
      <TouchableWithoutFeedback
        style={{ flexDirection: 'row', alignItems: 'center' }}
        onPress={() => {
          onCategorySelected(category);
          setViewData({ isModalVisible: false, categorySelected: index });
        }}
      >
        <View style={{ width: 30, height: 30, alignItems: 'center' }}>
          <Icon name={category.icon} size={25} color={'gold'} />
        </View>
        <Text white h3 middle style={{ padding: 10 }}>
          {category.title}
        </Text>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <View>
      <TouchableOpacity
        style={[style, styles.category]}
        onPress={() => setViewData({ ...viewData, isModalVisible: true })}
      >
        <Icon
          name={categories[viewData.categorySelected].icon}
          size={20}
          color={'gold'}
          style={{ marginHorizontal: 10 }}
        />
        <Text h3 white>
          {categories[viewData.categorySelected].title}
        </Text>
      </TouchableOpacity>

      <View>
        <Modal
          backdropOpacity={0.3}
          style={styles.contentView}
          isVisible={viewData.isModalVisible}
          onBackdropPress={() => setViewData({ ...viewData, isModalVisible: false })}
        >
          {renderOptions()}
        </Modal>
      </View>
    </View>
  );
};

export default CategoryView;
