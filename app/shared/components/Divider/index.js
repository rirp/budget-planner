import React, { Component } from 'react';

import Block from '../Block';
import { theme } from '../../constants';
import styles from './styles';

export default class Divider extends Component {
  render() {
    const { color, style, ...props } = this.props;
    const dividerStyles = [styles.divider, style];

    return <Block color={color || theme.colors.gray2} style={dividerStyles} {...props} />;
  }
}
