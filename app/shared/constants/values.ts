export const paymentTypes = [
  {
    value: '1',
    label: 'Credit card',
  },
  {
    value: '2',
    label: 'Payment utilities',
  },
  {
    value: '3',
    label: 'Home',
  },
  {
    value: '4',
    label: 'Entertainment',
  },
  {
    value: '5',
    label: 'Food and drinks',
  },
  {
    value: '6',
    label: 'Health',
  },
  {
    value: '7',
    label: 'Transportation',
  },
  {
    value: '8',
    label: 'Other',
  },
];

export const banks = [
  {
    value: '1',
    label: 'Bancolombia',
  },
  {
    value: '2',
    label: 'BBVA',
  },
  {
    value: '3',
    label: 'Davivienda',
  },
  {
    value: '4',
    label: 'Nequi',
  },
];
