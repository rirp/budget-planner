const cleanNumber = (value: string): number => {
  return parseInt(value.replace(/\D/g, ''));
};

const getCurrencyFormat = (value: number): string => {
  return new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0,
  }).format(value);
};

export { getCurrencyFormat, cleanNumber };
