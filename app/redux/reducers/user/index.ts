import produce from 'immer';
import { PayloadId } from '../../actions/types';
import { AppState, User } from '../../store/types';

export function createUserReducer(state: AppState, payload: User) {
  return produce(state, (draft) => {
    draft.users.push({
      id: payload.id ? payload.id : state.users.length + 1,
      name: payload.name,
      currentBalance: payload.currentBalance,
    });
    draft.selectedUser = draft.users[0];
  });
}

export function createUsersReducer(state: AppState, payload: User[]) {
  return produce(state, (draft) => {
    const updated = draft.users.concat(payload);
    draft.users = updated;
    draft.selectedUser = draft.users[0];
  });
}

export function selectUserReducer(state: AppState, payload: User) {
  return produce(state, (draft) => {
    draft.selectedUser = payload;
  });
}

export function updateUserReducer(state: AppState, payload: User) {
  return produce(state, (draft) => {
    const index = draft.users.findIndex((user) => user.id === payload.id);
    if (index != -1) {
      draft.users[index].name = payload.name;
    }
  });
}

export function removeUserReducer(state: AppState, payload: PayloadId) {
  return produce(state, (draft) => {
    const index = draft.users.findIndex((user) => user.id === payload.id);
    if (index != -1) draft.users.splice(index, 1);
  });
}
