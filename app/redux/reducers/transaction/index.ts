import produce from 'immer';
import { AppState, Transaction } from '../../store/types';

export const addTransaction = (state: AppState, transaction: Transaction) => {
  return produce(state, (draft) => {
    draft.transactions.push({
      ...transaction,
      id: transaction.id ? transaction.id : draft.transactions.length + 1,
    });
  });
};
