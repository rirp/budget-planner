export interface User {
  id?: number;
  name: string;
  currentBalance: number;
}

export interface Account {
  id: number;
  name: string;
  currentBalance: number;
  userId: number;
  transactions: Transaction[];
}

export interface Transaction {
  id?: number;
  description: string;
  amount: number;
  isIncomes: boolean;
  date: string;
  categoryId: number;
  userId: number;
}

export interface Category {
  key: string;
  title: string;
  icon: string;
  color: string;
}

export const categories: Category[] = [
  { key: '1', title: 'Utitilty', icon: 'flash', color: 'yellow' },
  { key: '2', title: 'Credit card', icon: 'credit-card', color: 'red' },
  { key: '3', title: 'Home', icon: 'home', color: 'blue' },
  { key: '4', title: 'Vehicule', icon: 'automobile', color: 'red' },
  { key: '5', title: 'Gasoline', icon: 'tachometer', color: 'red' },
  { key: '6', title: 'Education', icon: 'graduation-cap', color: 'red' },
  { key: '7', title: 'Healt', icon: 'stethoscope', color: 'green' },
  { key: '8', title: 'Food & drinks', icon: 'glass', color: 'red' },
  { key: '9', title: 'Groceries', icon: 'shopping-cart', color: 'red' },
];

export interface AppState {
  users: User[];
  transactions: Transaction[];
  selectedUser?: User;
}

export const initialState: AppState = {
  users: [],
  transactions: [],
};
