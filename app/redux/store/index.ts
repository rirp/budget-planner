import { createStore, applyMiddleware } from 'redux';
import { reducer } from '../reducers';
import thunk from 'redux-thunk';

/**
 * Instance of store using createStore by redux
 */
export const store = createStore(reducer, applyMiddleware(thunk));
