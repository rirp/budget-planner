import { AppState, Transaction } from '../../store/types';
import { ActionBuilder } from '../types';

export enum TransactionAction {
  AddTranscation = 'ADD_TRANSACTION',
}

export type AddTransactionAction = ActionBuilder<
  TransactionAction.AddTranscation,
  Transaction,
  AppState
>;

export type TransactionActions = AddTransactionAction;
