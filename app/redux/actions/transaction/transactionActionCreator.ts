import { Transaction } from '../../store/types';
import { addTransaction } from '../../reducers/transaction';
import { AddTransactionAction, TransactionActions, TransactionAction } from './transactionAction';

// ACTION CREATORS
export const addTransactionCreator = (payload: Transaction): AddTransactionAction => ({
  type: TransactionAction.AddTranscation,
  payload,
  reducer: addTransaction,
});
