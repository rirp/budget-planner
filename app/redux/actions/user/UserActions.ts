import { User, AppState } from '../../store/types';
import { ActionBuilder, PayloadId } from '../types';

export enum UserActionTypes {
  CREATE_USER = 'CREATE_USER',
  CREATE_USERS = 'CREATE_USERS',
  UPDATE_USER = 'UPDATE_USER',
  DELETE_USER = 'DELETE_USER',
  SELECT_USER = 'SELECT_USER',
}

export type CreateUserAction = ActionBuilder<UserActionTypes.CREATE_USER, User, AppState>;

export type CreateUsersAction = ActionBuilder<UserActionTypes.CREATE_USERS, User[], AppState>;

export type UpdateUserAction = ActionBuilder<UserActionTypes.UPDATE_USER, User, AppState>;

export type DeleteUserAction = ActionBuilder<UserActionTypes.DELETE_USER, PayloadId, AppState>;

export type SelectUserAction = ActionBuilder<UserActionTypes.SELECT_USER, User, AppState>;
