import { User } from '../../store/types';
import {
  CreateUserAction,
  UpdateUserAction,
  DeleteUserAction,
  UserActionTypes,
  CreateUsersAction,
  SelectUserAction,
} from './UserActions';
import {
  createUserReducer,
  createUsersReducer,
  removeUserReducer,
  updateUserReducer,
  selectUserReducer,
} from '../../reducers/user';

// ACTION CREATORS
export const createUser = (payload: User): CreateUserAction => ({
  type: UserActionTypes.CREATE_USER,
  payload,
  reducer: createUserReducer,
});

export const createUsers = (payload: User[]): CreateUsersAction => ({
  type: UserActionTypes.CREATE_USERS,
  payload,
  reducer: createUsersReducer,
});

export const updateUser = (payload: User): UpdateUserAction => ({
  type: UserActionTypes.UPDATE_USER,
  payload,
  reducer: updateUserReducer,
});

export const deleteUser = (id: number): DeleteUserAction => ({
  type: UserActionTypes.DELETE_USER,
  payload: { id },
  reducer: removeUserReducer,
});

export const selectUser = (user: User): SelectUserAction => ({
  type: UserActionTypes.SELECT_USER,
  payload: user,
  reducer: selectUserReducer,
});
