import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { CreateUserScreen } from '../screens';
import MainStackScreen from './MainStackScreen';
import AppLoading from 'expo-app-loading';
import { AppState } from '../redux/store/types';
import { getUsers } from '../middlewares/userMiddleware';
import { setup } from '../database';
import navTheme from './theme';

const RootStack = createStackNavigator();

export default () => {
  const [onFinish, setOnFinish] = useState(false);
  const dispatch = useDispatch();
  const appState = useSelector<AppState, AppState>((state) => state);

  const fetchUser = () => {
    dispatch(getUsers());
    setOnFinish(true);
  };

  if (!onFinish)
    return <AppLoading startAsync={setup} onFinish={() => fetchUser()} onError={console.warn} />;

  return (
    <NavigationContainer theme={navTheme}>
      <RootStack.Navigator headerMode="none">
        {appState.users == undefined || appState.users.length == 0 ? (
          <RootStack.Screen name="FirstOpen" component={CreateUserScreen} />
        ) : (
          <RootStack.Screen
            name="Main"
            component={MainStackScreen}
            options={{
              animationEnabled: false,
            }}
          />
        )}
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
