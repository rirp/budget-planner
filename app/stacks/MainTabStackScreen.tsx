import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AddTabButton } from '../shared/components/custom';
import { TransactionListScreen } from '../screens';
import HomeStackScreen from './HomeStackScreen';
import { theme } from '../shared/constants';

const Tab = createBottomTabNavigator();

const AddScreenComponent = () => {
  return null;
};

export default () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        labelStyle: {
          fontSize: 15,
        },
        activeTintColor: theme.colors.primary,
        inactiveTintColor: theme.colors.white,
        activeBackgroundColor: theme.colors.darkGray,
        inactiveBackgroundColor: theme.colors.darkGray,
        style: {
          backgroundColor: theme.colors.darkGray,
          borderColor: theme.colors.darkGray,
        },
      }}
    >
      <Tab.Screen name="Home" component={HomeStackScreen} />
      <Tab.Screen
        name="add"
        component={AddScreenComponent}
        options={{
          tabBarButton: () => <AddTabButton />,
        }}
      />
      <Tab.Screen name="Transaction" component={TransactionListScreen} />
    </Tab.Navigator>
  );
};
