import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MainTabStackScreen from './MainTabStackScreen';
import { AddTransactionScreen, CreateUserScreen } from '../screens';

const MainStack = createStackNavigator();

export default () => {
  return (
    <MainStack.Navigator screenOptions={{ headerBackTitleVisible: false }}>
      <MainStack.Screen
        name="HomeTab"
        component={MainTabStackScreen}
        options={{
          animationEnabled: false,
          headerShown: false,
        }}
      />
      <MainStack.Screen
        name="AddTransaction"
        component={AddTransactionScreen}
        options={{ title: 'Add transaction' }}
      />
      <MainStack.Screen
        name="CreateUser"
        component={CreateUserScreen}
        options={{ title: 'Create user' }}
      />
    </MainStack.Navigator>
  );
};
