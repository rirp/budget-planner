import { DefaultTheme } from '@react-navigation/native';
import { theme } from '../shared/constants';

const navTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: theme.colors.darkGray,
    primary: theme.colors.tertiary,
    card: theme.colors.darkGray,
    text: theme.colors.white,
  },
};

export default navTheme;
