import { StyleSheet } from 'react-native';
import { theme } from '../../shared/constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.black,
  },
  cards: {
    color: '#343434',
  },
  optionContainer: {
    flex: 0,
    marginLeft: 15,
  },
  input: {
    paddingLeft: 8,
    marginHorizontal: 10,
    borderRadius: 5,
    color: theme.colors.white,
    fontSize: 25,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  option: {
    marginRight: 10,
  },
  descriptionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15,
    alignSelf: 'stretch',
  },
  description: {
    marginLeft: 10,
    fontSize: 18,
    width: '100%',
  },
  amountIcon: {
    marginLeft: 5,
    paddingVertical: 3,
    paddingHorizontal: 5,
  },
  dateContainer: {
    justifyContent: 'center',
    paddingVertical: 8,
  },
  categoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  categoryIcon: {
    marginTop: 5,
    marginRight: 5,
    marginLeft: 15,
    alignSelf: 'center',
  },
  saveButton: {
    width: '90%',
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 30,
    backgroundColor: theme.colors.primary,
    marginBottom: 36,
  },
});

export default styles;
