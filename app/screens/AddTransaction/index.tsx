import React, { useState } from 'react';
import { KeyboardAvoidingView, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { Block, Input, Switch, Text, CategoryView } from '../../shared/components';
import {
  CurrencyInput,
  AddTransactionDateOption as DateOption,
} from '../../shared/components/custom';
import styles from './styles';
import { AddTransactionProps } from './types';
import { theme } from '../../shared/constants';
import { today, isToday, yesterday, isYesterday } from '../../shared/date';
import Icon from 'react-native-vector-icons/Feather';
import { Category, categories } from '../../redux/store/types';
import { insertTransaction } from '../../middlewares/transactionMiddleware';

const AddTransactionScreen: React.FC = () => {
  const [transactionData, setTransactionData] = useState({
    currentBalance: 0,
    currentBalanceString: '',
    description: '',
    date: today(),
    isDatePickerVisible: false,
    categorySelected: categories[0],
  });

  const dispatch = useDispatch();
  // Navigation data
  const navigation = useNavigation();
  const route = useRoute<RouteProp<AddTransactionProps, 'Detail'>>();

  const hideDatePicker = () => {
    setTransactionData({ ...transactionData, isDatePickerVisible: false });
  };

  const handleConfirm = (date: Date) => {
    setTransactionData({
      ...transactionData,
      date: date.toISOString().split('T')[0],
      isDatePickerVisible: false,
    });
  };

  const renderInputBalance = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Icon name="dollar-sign" size={30} color={'#85bb65'} style={styles.amountIcon} />
        <CurrencyInput
          style={styles.input}
          placeholder="$ 0.0"
          placeholderTextColor={theme.colors.gray}
          value={transactionData.currentBalanceString}
          onChangeValue={(valueInt: number, valueStr: string) => {
            setTransactionData({
              ...transactionData,
              currentBalance: valueInt,
              currentBalanceString: valueStr,
            });
          }}
        />
        <Switch value={route.params.isIcome} />
      </View>
    );
  };

  const renderDateButtons = (currentDate: string) => {
    return (
      <View style={{ flexDirection: 'row' }}>
        <DateOption
          title="Today"
          isSelected={isToday(currentDate)}
          onPressed={() => setTransactionData({ ...transactionData, date: today() })}
        />
        <DateOption
          title="Yesterday"
          isSelected={isYesterday(currentDate)}
          onPressed={() => setTransactionData({ ...transactionData, date: yesterday() })}
        />
        <DateOption
          title="Other"
          isSelected={false}
          onPressed={() => setTransactionData({ ...transactionData, isDatePickerVisible: true })}
        />
      </View>
    );
  };

  const renderDescription = () => {
    return (
      <View style={styles.descriptionContainer}>
        <Icon name="edit-2" size={25} color={theme.colors.white} />
        <Input
          placeholder="Description"
          placeholderTextColor="lightgray"
          style={styles.description}
          color={theme.colors.white}
          onChangeText={(text: string) =>
            setTransactionData({ ...transactionData, description: text })
          }
        />
      </View>
    );
  };

  const renderDateOptions = () => {
    const currentDate = transactionData.date;
    return (
      <View style={styles.dateContainer}>
        <Block row center style={styles.optionContainer}>
          <Icon name="calendar" size={30} color={theme.colors.white} style={{ marginRight: 5 }} />
          <DateTimePickerModal
            isVisible={transactionData.isDatePickerVisible}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />

          {isToday(currentDate) || isYesterday(currentDate) ? (
            renderDateButtons(currentDate)
          ) : (
            <TouchableOpacity
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => setTransactionData({ ...transactionData, isDatePickerVisible: true })}
            >
              <Text white h2>
                {transactionData.date}
              </Text>
            </TouchableOpacity>
          )}
        </Block>
      </View>
    );
  };

  const renderCategory = () => {
    return (
      <View style={styles.categoryContainer}>
        <Icon name="bookmark" size={30} color={theme.colors.white} style={styles.categoryIcon} />
        <CategoryView
          categories={categories}
          style={{ marginLeft: 10 }}
          onCategorySelected={(categorySelected: Category) =>
            setTransactionData({ ...transactionData, categorySelected })
          }
        />
      </View>
    );
  };

  const onSave = () => {
    dispatch(
      insertTransaction({
        description: transactionData.description,
        amount: transactionData.currentBalance,
        date: transactionData.date,
        categoryId: parseInt(transactionData.categorySelected.key),
        isIncomes: true,
        userId: 0,
      })
    );
    navigation.goBack();
  };

  const renderSaveButton = () => {
    return (
      <TouchableOpacity style={styles.saveButton} onPress={onSave}>
        <Text h1 white>
          Save
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <KeyboardAvoidingView behavior="padding" style={styles.container}>
      <Block>
        {renderInputBalance()}
        <Block color={theme.colors.darkGray} style={{ borderRadius: 30, paddingTop: 25 }}>
          {renderDateOptions()}
          {renderDescription()}
          {renderCategory()}
          {renderSaveButton()}
        </Block>
      </Block>
    </KeyboardAvoidingView>
  );
};

export default AddTransactionScreen;
