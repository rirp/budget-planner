import { StyleSheet } from 'react-native';
import { theme } from '../../shared/constants';

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: theme.colors.black,
  },
});

export default styles;
