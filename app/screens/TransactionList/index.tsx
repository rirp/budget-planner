import React from 'react';

import { Block, Text, Card } from '../../shared/components';
import { SafeAreaView, FlatList, StatusBar } from 'react-native';
import { theme } from '../../shared/constants';
import styles from './styles';
import { useSelector } from 'react-redux';
import { AppState, Transaction } from '../../redux/store/types';
import { getCurrencyFormat } from '../../shared/format';

const Item = ({ data }: { data: Transaction }) => (
  <Card color={theme.colors.darkGray}>
    <Block row>
      <Block>
        <Text white h2>
          {data.description}
        </Text>
        <Text white>{data.date}</Text>
      </Block>

      <Text white h2>
        {getCurrencyFormat(data.amount)}
      </Text>
    </Block>
  </Card>
);

export default () => {
  const appState = useSelector<AppState, AppState>((state) => state);
  const renderItem = (transaction: Transaction) => <Item data={transaction} />;

  return (
    <SafeAreaView style={styles.safeArea}>
      <StatusBar barStyle="light-content" />

      <Block padding={[0, theme.sizes.base]}>
        <FlatList
          data={appState.transactions.length > 0 ? appState.transactions : []}
          renderItem={(item) => renderItem(item.item)}
          keyExtractor={(item) => (item.id ? item.id.toString() : '0')}
        />
      </Block>
    </SafeAreaView>
  );
};
