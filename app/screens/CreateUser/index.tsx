import React, { useState } from 'react';
import { KeyboardAvoidingView, Platform } from 'react-native';
import { Block, Button, Input, Text } from '../../shared/components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { theme } from '../../shared/constants';
import styles from './styles';
import { useDispatch } from 'react-redux';
import { CurrencyInput } from '../../shared/components/custom';
import { saveUser } from '../../middlewares/userMiddleware';
import { useNavigation } from '@react-navigation/native';

interface IUserData {
  name: string;
  currentBalance: number;
  currentBalanceString: string;
}

const CreateUserScreen: React.FC = () => {
  const [userData, setUserData] = useState<IUserData>({
    name: '',
    currentBalance: 0,
    currentBalanceString: '',
  });

  const dispatch = useDispatch();
  const navigation = useNavigation();

  function isEnable() {
    return userData.name != null && userData.name != '';
  }

  return (
    <SafeAreaView style={styles.safeArea}>
      <KeyboardAvoidingView
        style={styles.keyboardView}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
      >
        <Block middle padding={[80, theme.sizes.base * 2]}>
          <Text white h1 center style={{ marginBottom: 25 }}>
            Create a person using its name and current balance
          </Text>
          <Input
            style={styles.input}
            placeholder="Name"
            value={userData.name}
            onChangeText={(name: string) => setUserData({ ...userData, name })}
          />
          <CurrencyInput
            style={styles.input}
            placeholder="Current balance"
            value={userData.currentBalanceString}
            onChangeValue={(valueInt: number, valueStr: string) => {
              setUserData({
                ...userData,
                currentBalance: valueInt,
                currentBalanceString: valueStr,
              });
            }}
          />
          <Button
            gradient
            opacity={0.5}
            disabled={!isEnable()}
            onPress={() => {
              dispatch(saveUser({ name: userData.name, currentBalance: userData.currentBalance }));
              if (navigation.canGoBack()) {
                navigation.goBack();
              }
            }}
          >
            <Text bold white center>
              Create user
            </Text>
          </Button>
        </Block>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default CreateUserScreen;
