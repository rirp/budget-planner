import { StyleSheet } from 'react-native';
import { theme } from '../../shared/constants';

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: theme.colors.black,
  },
  keyboardView: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    backgroundColor: theme.colors.white,
    paddingLeft: 5,
  },
});

export default styles;
