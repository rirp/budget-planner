import { StyleSheet } from 'react-native';
import { theme } from '../../shared/constants';

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: theme.colors.black,
  },
  cards: {
    color: '#343434',
  },
  addPersonButton: {
    height: 72,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    borderRadius: 10,
    backgroundColor: theme.colors.darkGray,
  },
  userContainer: {
    backgroundColor: theme.colors.darkGray,
    marginRight: 10,
  },
  selectedUser: {
    borderWidth: 1,
    borderColor: theme.colors.white,
  },
  userOptions: {
    marginTop: 10,
    flexDirection: 'row',
  },
});

export default styles;
