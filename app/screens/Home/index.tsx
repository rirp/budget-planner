import React from 'react';
import { Block, Text, Card } from '../../shared/components';
import { Overview } from '../../shared/components/custom';
import { SafeAreaView } from 'react-native-safe-area-context';
import { StatusBar, FlatList, ScrollView, TouchableOpacity, View } from 'react-native';
import { theme } from '../../shared/constants';
import styles from './styles';
import Icon from 'react-native-vector-icons/Feather';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { AppState, User } from '../../redux/store/types';
import { selectUser } from '../../redux/actions/user/UserActionsCreators';
import { getCurrencyFormat } from '../../shared/format';

const HomeScreen: React.FC = () => {
  const users = useSelector<AppState, User[]>((state) => state.users);
  const selectedUser = useSelector<AppState, User>((state) =>
    state.selectedUser ? state.selectedUser : { name: '', currentBalance: 0 }
  );
  const navigation = useNavigation<StackNavigationProp<any>>();
  const dispatch = useDispatch();

  const renderAddUserButton = () => {
    return (
      <TouchableOpacity
        onPress={() => navigation.push('CreateUser')}
        style={styles.addPersonButton}
      >
        <Icon name="user-plus" size={20} color={theme.colors.white} />
      </TouchableOpacity>
    );
  };

  const renderUserItem = (user: User) => {
    const selectedStyle = selectedUser.id == user.id ? styles.selectedUser : {};

    return (
      <TouchableOpacity onPress={() => dispatch(selectUser(user))}>
        <Card style={[styles.userContainer, selectedStyle]}>
          <Text white bold>
            {user.name}
          </Text>
          <Text white caption>
            {user.currentBalance ? getCurrencyFormat(user.currentBalance) : '$0'}
          </Text>
        </Card>
      </TouchableOpacity>
    );
  };

  const renderUsersSection = () => {
    return (
      <View style={styles.userOptions}>
        {renderAddUserButton()}
        <FlatList
          horizontal
          data={users}
          keyExtractor={(item) => `${item.id}`}
          renderItem={({ item }) => renderUserItem(item)}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <StatusBar barStyle="light-content" />

      <ScrollView>
        <Block padding={[0, theme.sizes.base]}>
          <Overview currentBalanace={selectedUser.currentBalance} style={{ marginTop: 10 }} />
          {renderUsersSection()}
        </Block>
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
