import DataOperation from './DataOperation';
import DatabaseLayer from './DatabaseLayer';
import { WebSQLDatabase } from 'expo-sqlite';
import { Column, Id, Options, Referece, Statement } from './types';

export default class Repository {
  private columnMapping: Column[];
  private referenceMapping?: Referece[];
  private databaseLayer: DatabaseLayer;

  constructor(
    database: WebSQLDatabase,
    tableName: string,
    columnMapping: Column[],
    referenceMapping?: Referece[]
  ) {
    this.columnMapping = columnMapping;
    this.referenceMapping = referenceMapping;
    this.databaseLayer = new DatabaseLayer(database, tableName);
  }

  createTable() {
    return this.databaseLayer.createTable(this.columnMapping, this.referenceMapping);
  }

  dropTable() {
    return this.databaseLayer.dropTable();
  }

  insert<T>(_obj: object) {
    return this.databaseLayer
      .insert(_obj)
      .then((res) => DataOperation.convertRowToObject<T>(res)[0]);
  }

  update<T extends Id>(_obj: T) {
    return this.databaseLayer
      .update(_obj)
      .then((res) => (res ? DataOperation.convertRowToObject<T>(res)[0] : null));
  }

  destroy(id: number) {
    return this.databaseLayer.destroy(id);
  }

  destroyAll() {
    return this.databaseLayer.destroyAll();
  }

  find<T>(id: number) {
    return this.databaseLayer
      .find(id)
      .then((res) => (res ? DataOperation.convertRowToObject<T>(res)[0] : null));
  }

  findBy<T>(options: Options) {
    return this.databaseLayer
      .findBy(options)
      .then((res) => (res ? DataOperation.convertRowToObject<T>(res) : null));
  }

  query<T>(options: Options) {
    return this.databaseLayer
      .query(options)
      .then((res) => DataOperation.convertRowToObject<T>(res));
  }
}
