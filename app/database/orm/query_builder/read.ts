import { Options, Operations, Statement, Order } from '../types';

const defaultOptions: Options = {
  columns: '*',
  page: undefined,
  limit: 30,
  where: [],
  order: Order.ACS,
};

// Creates the "SELECT" sql statement for find one record
export function find(tableName: string) {
  return `SELECT * FROM ${tableName} WHERE id = ? LIMIT 1;`;
}

/* Creates the "SELECT" sql statement for query records
 * Ex: qb.query({
 *   columns: 'id, nome, status',
 *   where: {status_eq: 'encerrado'}
 * })
 */
export function query(tableName: string, options: Options = defaultOptions) {
  const { columns, page, limit, where, order } = {
    ...defaultOptions,
    ...options,
  };

  const whereStatement = where !== undefined ? queryWhere(where) : '';
  let sqlParts = ['SELECT', columns, 'FROM', tableName, whereStatement, 'ORDER BY', order];

  if (page !== undefined) {
    sqlParts.push(...['LIMIT', limit.toString(), 'OFFSET', (limit * (page - 1)).toString()]);
  }
  return sqlParts.filter((p) => p !== '').join(' ');
}

// Convert operators to database syntax
export function propertyOperation(statement: Statement) {
  const operation = statement.operation;
  const property = statement.column;

  return operation && property ? `${property} ${operation}` : null;
}

// Build where query
export function queryWhere(options: Statement[]) {
  const list = options.map((p) => `${propertyOperation(p)} ?`);
  return list.length > 0 ? `WHERE ${list.join(' AND ')}` : '';
}

export default { find, query };
