import { Column, ColumnType, Referece } from '../types';

export const customTypes = { Json: 'TEXT' };

/* Creates a string with the columns to create a table like:
 *  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT, age INTEGER
 */
export function _createTableColumns(columns: Column[]): string {
  return columns
    .map((column) => {
      const type = column.type === ColumnType.Json ? ColumnType.Text : column.type;
      const parts: string[] = [column.name, type];

      if (column.primaryKey) {
        parts.push('NOT NULL PRIMARY KEY AUTOINCREMENT');
      } else {
        if (column.unique) parts.push('UNIQUE');
        if (column.noNull) parts.push('NOT NULL');
      }
      return parts.join(' ');
    })
    .join(', ');
}

export function _createTableReferences(references: Referece[]): string | null {
  const refs = references
    .map((r) => {
      const tableRef = r.tableRef;
      const columnRef = r.columnRef;
      return `FOREIGN KEY(${columnRef}) REFERENCES ${tableRef}(id)`;
    })
    .join(', ');
  return refs === '' ? null : refs;
}

// Creates the "CREATE TABLE" sql statement
export function createTable(
  tableName: string,
  columnMapping: Column[],
  referenceMapping?: Referece[]
): string {
  const columns = _createTableColumns(columnMapping);
  var references = undefined;

  if (referenceMapping !== undefined) {
    _createTableReferences(referenceMapping);
  }

  const colRef = [columns, references].filter(Boolean).join(', ');

  return `CREATE TABLE IF NOT EXISTS ${tableName} (${colRef});`;
}

// Creates the "DROP TABLE" sql statement
export function dropTable(tableName: string) {
  return `DROP TABLE IF EXISTS ${tableName};`;
}

export default { createTable, dropTable };
