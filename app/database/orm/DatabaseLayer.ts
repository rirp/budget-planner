import { SQLError, SQLTransaction, WebSQLDatabase } from 'expo-sqlite';
import QueryBuilder from './query_builder';
import {
  Id,
  Column,
  Options,
  Referece,
  Statement,
  Sql,
  SqleResolve,
  SqlReject,
  SqlResult,
} from './types';

export default class DatabaseLayer {
  private database: WebSQLDatabase;
  private tableName: string;

  constructor(database: WebSQLDatabase, tableName: string) {
    this.database = database;
    this.tableName = tableName;
  }

  async executeBulkSql({ sqls, params = [] }: Sql) {
    const database = await this.database;
    return new Promise<SqlResult[]>((txResolve, txReject: SqlReject) => {
      database.transaction((tx) => {
        Promise.all(
          sqls.map((sql, index) => {
            return new Promise((sqlResolve: SqleResolve, sqlReject: SqlReject) => {
              tx.executeSql(
                sql,
                params[index],
                (_, { rows, insertId }) => {
                  sqlResolve({ rows: rows, insertId });
                },
                (_: SQLTransaction, error: SQLError) => {
                  sqlReject(error);
                  return false;
                }
              );
            });
          })
        )
          .then(txResolve)
          .catch(txReject);
      });
    });
  }

  async executeSql(sql: string, params: any = []) {
    return this.executeBulkSql({ sqls: [sql], params: [params] })
      .then((res: SqlResult[]) => {
        return res[0];
      })
      .catch((error) => {
        throw error;
      });
  }

  createTable(columnMapping: Column[], referenceMapping?: Referece[]) {
    const sql = QueryBuilder.createTable(this.tableName, columnMapping, referenceMapping);
    return this.executeSql(sql).then(() => true);
  }

  dropTable() {
    const sql = QueryBuilder.dropTable(this.tableName);
    return this.executeSql(sql).then(() => true);
  }

  insert(obj: object) {
    const sql = QueryBuilder.insert(this.tableName, obj);
    const params = Object.values(obj);
    return this.executeSql(sql, params).then(({ insertId }) =>
      this.find(insertId).then((res) => res)
    );
  }

  update<T extends Id>(obj: T) {
    const sql = QueryBuilder.update(this.tableName, obj);
    const { id, ...props } = obj;
    const params = Object.values(props);
    return this.executeSql(sql, [...params, id]).then(({ rows }) => rows);
  }

  bulkInsertOrReplace(objs: Sql[]) {
    const list = objs.reduce(
      (accumulator, obj) => {
        const params = Object.values(obj);
        accumulator.sqls.push(QueryBuilder.insertOrReplace(this.tableName, obj));
        accumulator.params.push(params);
        return accumulator;
      },
      { sqls: [], params: [] }
    );
    return this.executeBulkSql({ sqls: list.sqls, params: list.params }).then((res) => res);
  }

  destroy(id: number) {
    const sql = QueryBuilder.destroy(this.tableName);
    return this.executeSql(sql, [id]).then(() => true);
  }

  destroyAll() {
    const sql = QueryBuilder.destroyAll(this.tableName);
    return this.executeSql(sql).then(() => true);
  }

  find(id: number) {
    const sql = QueryBuilder.find(this.tableName);
    return this.executeSql(sql, [id]).then(({ rows }) => rows);
  }

  findBy(options: Options = { limit: 1 }) {
    const sql = QueryBuilder.query(this.tableName, options);
    const params = options.where;
    return this.executeSql(sql, params).then(({ rows }) => rows);
  }

  query(options: Options) {
    const sql = QueryBuilder.query(this.tableName, options);
    const params = Object.values(options.where || {});
    return this.executeSql(sql, params).then(({ rows }) => rows);
  }
}
