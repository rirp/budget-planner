import BaseModel from './BaseModel';
import DataOperation from './DataOperation';

export { BaseModel, DataOperation };
export default { BaseModel, DataOperation };
