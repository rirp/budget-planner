import { SQLError, SQLResultSetRowList } from 'expo-sqlite';

export type defaultCallback = () => any;
export type SqlResult = { rows: SQLResultSetRowList; insertId: number };
export type SqleResolve = (value: SqlResult) => void;
export type SqlReject = (error: SQLError) => void;
export type Sql = { sqls: string[]; params: any[] };

export type Id = {
  id?: number;
};

export enum ColumnType {
  Integer = 'INTEGER',
  Float = 'FLOAT',
  Text = 'TEXT',
  Numeric = 'NUMERIC',
  Date = 'DATE',
  Datetime = 'DATETIME',
  Boolean = 'BOOLEAN',
  Json = 'JSON',
}

export enum Operations {
  eq = '=',
  neq = '<>',
  lt = '<',
  lteq = '<=',
  gt = '>',
  gteq = '>=',
  cont = 'LIKE',
}

export enum Order {
  ACS = 'id ASC',
  DESC = 'id DESC',
}

export interface Options {
  columns?: string;
  page?: number;
  limit: number;
  where?: Statement[];
  order?: Order;
}

export interface Statement {
  column: string;
  operation: Operations;
  value: any;
}

export interface Column {
  name: string;
  type: ColumnType;
  primaryKey?: boolean;
  unique?: boolean;
  noNull?: boolean;
  default?: defaultCallback;
}

export interface Referece {
  tableRef: string;
  columnRef: string;
}
