import Repository from './Repository';
import { DatabaseConnection } from '../database-conexion';
import { Column, Id, Options, Referece, Statement } from './types';

export interface TableProps {
  tableName: string;
  columnMapping: Column[];
  referenceMapping?: Referece[];
}

type Constructor<T> = new (...args: any[]) => T;

export default function BaseModel<T extends Constructor<{}>>(Base: T, props: TableProps) {
  return class Table extends Base {
    id?: number;

    constructor(...args: any[]) {
      super(...args);
    }

    static get database() {
      return DatabaseConnection.getConnection();
    }

    static get repository() {
      return new Repository(
        this.database,
        this.tableName,
        this.columnMapping,
        this.referenceMapping
      );
    }

    static get tableName(): string {
      return props.tableName;
    }

    static get columnMapping(): Column[] {
      return props.columnMapping;
    }

    static get referenceMapping(): Referece[] | undefined {
      return props.referenceMapping;
    }

    static createTable() {
      return this.repository.createTable();
    }

    static dropTable() {
      return this.repository.dropTable();
    }

    static create<U extends Id>(obj: U) {
      return this.repository.insert(obj).then((res) => res);
    }

    static update<U extends Id>(obj: U) {
      return this.repository.update(obj).then((res) => res);
    }

    save() {
      if (this.id) {
        return Table.repository.update(this);
      } else {
        return Table.repository.insert(this);
      }
    }

    destroy() {
      if (this.id) {
        return Table.repository.destroy(this.id);
      } else {
        return false;
      }
    }

    static destroy(id: number) {
      return this.repository.destroy(id);
    }

    static destroyAll() {
      return this.repository.destroyAll();
    }

    static find<T>(id: number) {
      return this.repository.find<T>(id).then((res) => res);
    }

    static findBy<T>(options: Options) {
      return this.repository.findBy<T>(options).then((res) => res);
    }

    /**
     * @param {columns: '*', page: 1, limit: 30, where: {}, order: 'id DESC'} options
     */
    static query(options: Options) {
      return this.repository.query(options);
    }
  };
}
