import { SQLResultSetRowList } from 'expo-sqlite';

function convertRowToObject<T>(rows: SQLResultSetRowList): T[] {
  const array: T[] = [];
  for (var _i = 0; _i < rows.length; _i++) {
    array.push(rows.item(_i));
  }
  return array;
}

export default {
  convertRowToObject,
};
