import AccountEntity from './entities/AccountEntity';
import PersonEntity from './entities/PersonEntity';
import AsyncStorage from '@react-native-community/async-storage';

const CONFIGURED_TASK_COUNTER = 'configuredTaskCounter';

const tasks = [PersonEntity.createTable(), AccountEntity.createTable()];

async function setup() {
  return Promise.all(tasks).then(() => {});
}

function _executeTask(callback: (progress: number) => void) {}

async function _setConfiguredTaskCounter(count: number) {
  try {
    await AsyncStorage.setItem(CONFIGURED_TASK_COUNTER, count.toString());
    return true;
  } catch {
    return false;
  }
}

async function _getConfiguredTaskCounter() {
  try {
    let value = await AsyncStorage.getItem(CONFIGURED_TASK_COUNTER);
    if (value != null) {
      return parseInt(value);
    }
    return 0;
  } catch {
    return -1;
  }
}

export { setup };
