import { BaseModel } from '../orm';
import { ColumnType } from '../orm/types';

class Category {
  name = '';
}

const CategoryEntity = BaseModel(Category, {
  tableName: 'category',
  columnMapping: [
    {
      name: 'id',
      type: ColumnType.Integer,
      primaryKey: true,
    },
    {
      name: 'name',
      type: ColumnType.Text,
      noNull: true,
    },
  ],
});

export default CategoryEntity;
