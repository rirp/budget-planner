import { BaseModel } from '../orm';
import { ColumnType } from '../orm/types';

class Person {
  name?: string;
  currentBalance?: number;
}

const PersonEntity = BaseModel(Person, {
  tableName: 'person',
  columnMapping: [
    { name: 'id', type: ColumnType.Integer, primaryKey: true },
    { name: 'name', type: ColumnType.Text, noNull: true },
    { name: 'currentBalance', type: ColumnType.Float, noNull: true },
  ],
});

export default PersonEntity;
