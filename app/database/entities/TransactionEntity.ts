import { BaseModel } from '../orm';
import { ColumnType } from '../orm/types';

class Transaction {
  amount = 0.0;
  isIncomes = true;
  description = '';
  date = '';
  categoryId = 0;
  userId = 0;
}

const TransactionEntity = BaseModel(Transaction, {
  tableName: 'transaction',
  columnMapping: [
    { name: 'id', type: ColumnType.Integer, primaryKey: true },
    { name: 'amount', type: ColumnType.Float, noNull: true },
    { name: 'description', type: ColumnType.Text, noNull: true },
    { name: 'isIncomes', type: ColumnType.Boolean, noNull: true },
    { name: 'date', type: ColumnType.Datetime, noNull: true },
    { name: 'categoryId', type: ColumnType.Integer, noNull: true },
    { name: 'userId', type: ColumnType.Integer, noNull: true },
  ],
});

export default TransactionEntity;
